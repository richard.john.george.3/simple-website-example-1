import Link from "next/link";

const App = () => (
    <div>
	    <h1>Hello World</h1>
		<Link href="/home">
		    <a>Home Page</a>
		</Link>
	</div>
);

export default App;
